# main.rb

# Method: Which file to process, either default file or passed in file
def file_to_process
  if ARGV.empty? 
    file_name = "test1.txt"
  else
    file_name = ARGV[0]
  end
end

# Method: Grab all lines in the given file and return it in an array
def self.array_of_numbers_from_file(file)
  # NOTE: would be nice to check if file exists
  yanked_from_file_array = Array.new
  file = File.new(file, "r")
  while (line = file.gets)
    yanked_from_file_array << line.chomp.to_i
  end
  file.close
  return yanked_from_file_array
end

# Method: Return array of sums that one number makes from a given array of numbers
def sums_with_all_numbers_that_follow_it_in_the_given_array(passed_in_number, full_array)
  position_after_passed_in_number = ( (full_array.index(passed_in_number)) + 1 ) 
  last_position = (full_array.count + 1)
  temp_sums = Array.new
  
  # Add the passed in number to each number that comes after it in the array
  # This assumes the numbers before that num already created a sum with that num
  full_array[position_after_passed_in_number..last_position].each do |this_addend|
    sum = (passed_in_number + this_addend)
    #puts "The sum of (#{passed_in_number} + #{this_addend}) is: #{sum}"
    if (sum >= -10000) && (sum <= 10000)
      temp_sums << sum
    end
  end
  return temp_sums
end


# ======================================================================
# Start application here
file_name = file_to_process
original_numbers = self.array_of_numbers_from_file(file_name)
unique_numbers = original_numbers.uniq
unique_numbers_count = unique_numbers.count
all_sums = Array.new

# Go through each unique # from the file, and collect the sums it makes
unique_numbers.each do |number|
  # NOTE: This thread implementation did NOT speed things up
  # Will have to find an improved or completely different solution
  # to try and speed things up. 
  x = Thread.new do
    found_sums = sums_with_all_numbers_that_follow_it_in_the_given_array(number, unique_numbers)
    if (found_sums.any?)
      all_sums.concat(found_sums)
    end
    # puts "#{unique_numbers_count -= 1} remaining items to evaluate"
  end
  x.join
end

puts "File '#{file_name.to_s}' has '#{all_sums.uniq.count}' distinct y values."
