def self.array_from_file(file)
  # NOTE: would be nice to check if file exists
  yanked_from_file_array = Array.new
  file = File.new(file, "r")
  while (line = file.gets)
    yanked_from_file_array << line.chomp.to_i
  end
  file.close
  return yanked_from_file_array
end

#file_name = "gaga.txt"
#file_name = "medium.txt"
#file_name = "test1.txt"
file_name = "test2.txt"
#file_name = "large.txt"

original_array = self.array_from_file(file_name)
unique_array = original_array.uniq
ua_count = unique_array.count
addend_b_array = unique_array.dup
sum_list = Array.new
evaluated_addends = 0

unique_array.each do |addend_a|
  #puts "Evaluating #{addend_a}"
  addend_b_array.delete(addend_a)
  addend_b_array.each do |addend_b|
    sum = (addend_a + addend_b)
    if (sum >= -10000) && (sum <= 10000)
      sum_list << sum
    end
  end
  evaluated_addends += 1
  #puts "#{ua_count - evaluated_addends} items left to evaluate"
end

puts "File '#{file_name.to_s}' has '#{sum_list.uniq.count}' distinct y values."
